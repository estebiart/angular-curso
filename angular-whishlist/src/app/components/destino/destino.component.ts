import { Component, OnInit, Input, HostBinding, EventEmitter,Output } from '@angular/core';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { VoteDownAction, VoteUpAction } from '../../models/destinos-viajes-state.model';
@Component({
  selector: 'app-destino',
  templateUrl: './destino.component.html',
  styleUrls: ['./destino.component.css']
})
export class DestinoComponent implements OnInit {
@Input() destino:DestinoViaje ;
@Input('idx') position:number;
@HostBinding('attr.class') cssClass ="col-md-4";
@Output() clicked:EventEmitter<DestinoViaje>;
  constructor(private store: Store <AppState>) { 
      this.clicked =new EventEmitter();
  } 

  ngOnInit() {
  }
   ir(){
     this.clicked.emit(this.destino);
     return false;
   }

   voteUp(){
     this.store.dispatch( new VoteUpAction(this.destino));
     return false;
   }
   voteDown(){
    this.store.dispatch( new VoteDownAction(this.destino));
    return false;
  }
}
