import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { ElegidoFavoritoAction, NuevoDestinoAction,reducerDestinosViajes } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html', 
  styleUrls: ['./lista.component.css'],
 providers:[ DestinosApiClient]
})
export class ListaComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  //destinos:DestinoViaje[];
  all;

  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded =new EventEmitter();
    this.updates=[];
     //store.select(state => state.destinos.items).subscribe(items => this.all = items);
   }
 
  ngOnInit(){

      this.store.select(state => state.destinos.favorito)
      .subscribe( data =>{
        const f = data;
        if(f !=null){
          this.updates.push('se ha elegido ' + f.nombre)
        } 
      });

  }

   agregado(d:DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
 
   }
   elegido(e:DestinoViaje){
    this.destinosApiClient.elegir(e);

   }
   getAll(){

   }
}


 
